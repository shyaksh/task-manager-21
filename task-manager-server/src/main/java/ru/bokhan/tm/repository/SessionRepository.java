package ru.bokhan.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.api.repository.ISessionRepository;
import ru.bokhan.tm.entity.Session;

import java.util.ArrayList;
import java.util.List;

public final class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    @NotNull
    @Override
    public List<Session> findByUserId(@NotNull final String userId) {
        @NotNull final List<Session> sessions = findAll();
        @NotNull final List<Session> result = new ArrayList<>();
        for (@Nullable final Session session : sessions) {
            if (session == null) continue;
            if (userId.equals(session.getUserId())) result.add((session));
        }
        return result;
    }

    @Override
    public void removeByUserId(@NotNull final String userId) {
        final List<Session> sessions = findByUserId(userId);
        for (final Session session : sessions) remove(session);
    }

    @Nullable
    @Override
    public Session findById(@NotNull String id) {
        for (@Nullable final Session session : entities) {
            if (session == null) continue;
            if (id.equals(session.getId()))
                return session;
        }
        return null;
    }

}
