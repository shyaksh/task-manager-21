package ru.bokhan.tm.command.data.binary;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.command.AbstractCommand;
import ru.bokhan.tm.endpoint.DomainEndpoint;
import ru.bokhan.tm.endpoint.Session;
import ru.bokhan.tm.exception.security.AccessDeniedException;

public final class DataBinaryClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "data-bin-clear";
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove binary data file.";
    }

    @Override
    public void execute() {
        @Nullable final Session session = endpointLocator.getCurrentSession();
        if (session == null) throw new AccessDeniedException();
        System.out.println("[DATA BINARY CLEAR]");
        @NotNull final DomainEndpoint domainEndpoint = endpointLocator.getDomainEndpoint();
        if (domainEndpoint.removeBinary(session)) System.out.println("[OK]");
    }

}
