package ru.bokhan.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.command.AbstractCommand;
import ru.bokhan.tm.endpoint.Session;
import ru.bokhan.tm.endpoint.User;
import ru.bokhan.tm.exception.security.AccessDeniedException;
import ru.bokhan.tm.util.TerminalUtil;

public final class ProfileUpdateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "profile-update";
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update my profile";
    }

    @Override
    public void execute() {
        @Nullable final Session session = endpointLocator.getCurrentSession();
        if (session == null) throw new AccessDeniedException();
        System.out.println("[UPDATE PROFILE]");
        System.out.println("ENTER NEW LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER NEW FIRST NAME:");
        @NotNull final String firstName = TerminalUtil.nextLine();
        System.out.println("ENTER NEW MIDDLE NAME:");
        @NotNull final String middleName = TerminalUtil.nextLine();
        System.out.println("ENTER NEW LAST NAME:");
        @NotNull final String lastName = TerminalUtil.nextLine();
        System.out.println("ENTER NEW EMAIL:");
        @NotNull final String email = TerminalUtil.nextLine();
        @Nullable final String userId = session.getUserId();
        @NotNull final User user = endpointLocator.getUserEndpoint().updateUserById(
                session, userId, login,
                firstName, lastName, middleName,
                email
        );
        if (user == null) System.out.println("[FAIL]");
        System.out.println("[OK]");
    }

}
