package ru.bokhan.tm.command;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.api.IEndpointLocator;
import ru.bokhan.tm.endpoint.Role;


@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractCommand {

    protected IEndpointLocator endpointLocator;

    public abstract String name();

    public abstract String argument();

    public abstract String description();

    public abstract void execute();

}
