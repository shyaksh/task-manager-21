package ru.bokhan.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.command.AbstractCommand;
import ru.bokhan.tm.endpoint.Project;
import ru.bokhan.tm.endpoint.Session;
import ru.bokhan.tm.exception.security.AccessDeniedException;

import java.util.List;

public final class ProjectListCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "project-list";
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show project list";
    }

    @Override
    public void execute() {
        @Nullable final Session session = endpointLocator.getCurrentSession();
        if (session == null) throw new AccessDeniedException();
        System.out.println("[LIST PROJECTS]");
        @NotNull final List<Project> projects = endpointLocator.getProjectEndpoint().findProjectAll(session);
        for (@Nullable final Project project : projects) System.out.println(project);
        System.out.println("[OK]");
    }

}
