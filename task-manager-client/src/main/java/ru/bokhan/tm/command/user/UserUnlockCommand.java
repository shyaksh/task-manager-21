package ru.bokhan.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.command.AbstractCommand;
import ru.bokhan.tm.endpoint.Role;
import ru.bokhan.tm.endpoint.Session;
import ru.bokhan.tm.exception.security.AccessDeniedException;
import ru.bokhan.tm.util.TerminalUtil;

public final class UserUnlockCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "user-unlock";
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Unlock user by login";
    }

    @Override
    public void execute() {
        @Nullable final Session session = endpointLocator.getCurrentSession();
        if (session == null) throw new AccessDeniedException();
        System.out.println("[UNLOCK USER]");
        System.out.println("ENTER LOGIN");
        @NotNull final String login = TerminalUtil.nextLine();
        endpointLocator.getUserEndpoint().unlockUserByLogin(session, login);
        System.out.println("[OK]");
    }

}
